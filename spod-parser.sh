#!/usr/bin/env bash
#                                                         !"#$%&'()*+,-./ {|}~ [\]^_`
t="$(mktemp)" charset="a-zA-Z~éùàèç§€µ²°¤¨öüäëïì0-9 ;:=?@\x21-\x2F\x7B-\x7E\x5B-\x60"
case $(uname) in
	FreeBSD)	sed="gsed";;
	*)			sed="sed";;
esac
declare -A SPOD_COLOR
SPOD_COLOR[B]="\e[38;5;15m"	# Bold
SPOD_COLOR[D]="\e[38;5;6m"	# Default value
SPOD_COLOR[O]="\e[38;5;14m"	# Option
SPOD_COLOR[V]="\e[38;5;3m"	# Value
SPOD_COLOR[0]="\e[0m"		# Reset
SPOD_COLOR[T]="\e[2m"		# HashTag
SPOD_COLOR[C]="\e[38;5;36m"	# Comment
function replace_tags() {
	local file="$1"
	# Replace O<plop> by <colorcode>plop<reset>
	for k in "${!SPOD_COLOR[@]}" ; do
		# Hide those
		$sed -Ee 's/"/#22/g' -e 's/\\/#5c/g' -e 's/`/#60/g' -i "$file"
		$sed -Ee "s/$k<([$charset]+)>/\${SPOD_COLOR[$k]}\1\${SPOD_COLOR[0]}/g" -i "$file"
	done
}
function replace_vars() {
	local file="$1" vars="$2"
	IFS="," ; set -- $vars
	# So that it also handle case with leading "," such as "-r ',A=b,C=d'
	for s in "$@" ; do
		IFS='=' read -r k v <<< "$s"
		$sed -e "s/__${k}__/${v}/g" -i "$file"
		shift
	done
}
function usage() {
	# Should spod-parser have a .spod help?
	echo "$(basename "$0") [ -r V=v[,V=v[...] ] <file>"
}
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
options="$(getopt -o "hr:" -- "$@")"
eval set -- $options
while true ; do
	case $1 in
		-h)	usage ; exit 0 ;;
		-r)	shift ; REPLACEMENT="$1" ;;
		--)	shift ; break ;;
	esac
	shift
done
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cp "$1" "$t"
replace_tags "$t"
if [ -n "$REPLACEMENT" ] ; then
	replace_vars "$t" "$REPLACEMENT"
fi
# Ugly...
$sed -e 's/^\(.*\)$/echo -e "\1"/' -i "$t"
# Replace back on display
source "$t" | $sed -e 's/#5c/\\/g' -e 's/#60/\`/g' -e 's/#22/"/g'
rm -f "$t"
