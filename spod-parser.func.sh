#!/usr/bin/env bash
function spod-parser() {
	#                                                                        !"#$%&'()*+,-./ {|}~ [\]^_`
	local file="$1" t="$(mktemp)" charset="a-zA-Z~éùàèç§€µ²°¤¨öüäëïì0-9 ;:=?@\x21-\x2F\x7B-\x7E\x5B-\x60" k
	case $(uname) in
		FreeBSD)	sed="gsed";;
		*)			sed="sed";;
	esac
	declare -A SPOD_COLOR
	SPOD_COLOR[B]="\e[1;37m"	# Bold
	SPOD_COLOR[D]="\e[7m"		# Default value
	SPOD_COLOR[O]="\e[36m"		# Option
	SPOD_COLOR[V]="\e[33m"		# Value
	SPOD_COLOR[0]="\e[0m"		# Reset
	SPOD_COLOR[T]="\e[2m"		# HashTag
	SPOD_COLOR[C]="\e[38;5;36m"	# Comment
	cp "$file" "$t"
	# Replace O<plop> by <colorcode>plop<reset>
	for k in "${!SPOD_COLOR[@]}" ; do
		$sed -Ee 's/"/\\x22/g' -i "$t"
		# Hide those$
		$sed -Ee 's/\\/#5c/g' -i "$t"
		$sed -Ee 's/`/#60/g' -i "$t"
		$sed -Ee "s/$k<([$charset]+)>/\${SPOD_COLOR[$k]}\1\${SPOD_COLOR[0]}/g" -i "$t"
	done
	# Ugly...
	$sed -e 's/^\(.*\)$/echo -e "\1"/' -i "$t"
	# Replace on display
	source "$t" | $sed -e 's/#5c/\\/g' -e 's/#60/\`/g'
	#cat "$t"
	rm -f "$t"
}
