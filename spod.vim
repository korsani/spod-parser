" Vim syntax file
" Language:      Perl POD format
" Maintainer:    vim-perl <vim-perl@googlegroups.com>
" Previously:    Scott Bigham <dsb@killerbunnies.org>
" Homepage:      http://github.com/vim-perl/vim-perl
" Bugs/requests: http://github.com/vim-perl/vim-perl/issues
" Last Change:   2017-09-12

" To add embedded POD documentation highlighting to your syntax file, add
" the commands:
"
"   syn include @Pod <sfile>:p:h/pod.vim
"   syn region myPOD start="^=pod" start="^=head" end="^=cut" keepend contained contains=@Pod
"
" and add myPod to the contains= list of some existing region, probably a
" comment.  The "keepend" flag is needed because "=cut" is matched as a
" pattern in its own right.


" Remove any old syntax stuff hanging around (this is suppressed
" automatically by ":syn include" if necessary).
" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim
" Bordel!
" hi clear

hi Normal NONE
syn region spodBold	matchgroup=spodCommand	start="B<"me=e end=">" oneline 
syn region spodDefault	matchgroup=spodCommand	start="D<"me=e end=">" oneline 
syn region spodOption	matchgroup=spodCommand	start="O<"me=e end=">" oneline 
syn region spodComment	matchgroup=spodCommand	start="C<"me=e end=">" oneline 
syn region spodTag	matchgroup=spodCommand	start="T<"me=e end=">" oneline 
syn region spodValue	matchgroup=spodCommand	start="V<"me=e end=">" oneline 
syn region spodVar	matchgroup=spodCommand	start="__"me=e end="__" oneline 

hi 		spodBold 	cterm=bold,underline	gui=bold,underline	term=bold,underline
" cyan
hi 		spodDefault 	ctermfg=6	guifg=#008080
hi		spodNormal	cterm=NONE	gui=NONE	term=NONE
hi		spodVar		cterm=italic	gui=italic	term=italic
" hi cyan
hi		spodOption	ctermfg=14	guifg=#00ffff
hi def	link	spodComment	Comment
hi def	link	spodValue	Type
hi	spodTag		guifg=purple	ctermfg=99
hi 	spodCommand	guifg=#5151ff	ctermfg=darkgrey
let b:current_syntax = "spod"

let &cpo = s:cpo_save
unlet s:cpo_save

" vim: ts=8
