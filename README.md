# spod-parser

## Name

The simple/shell (coloring) POD parser

## Description

POD is simple to understand and to write documentation with.

Writing shell script help file is a matter of dozens of ```echo``` lines, or use of ugly tricks (in-line doc with ```cat <<<EOF```, ```grep``` of itself) especially it you want colors in it.

```spod-parser``` allow you to write colorized POD-like documentation.

## Visuals

This :

```

    🧙 B<magic.sh> ✨

Usage:

    $ B<magic.sh> [ O<-h> ] O<-c> O<-r> V<file>

Options:

    O<-h>   : ask for help
    O<-c>   : set the switch O<-c>. It's D<off> by default
    O<-r>   : choose a random value. Default is D<__RANDOM__>

Example:

    $ B<magic.sh> O<-c> myfile      # C<create a magic file> T<#unicorn>

To do:

    Implement specials chars escaping, such as : V<'àçùéè ^#@()[]|{}~& " %*§!:;,µ²°+= _¤¨üöäëï`?€~/\>
```

Is rendered :

![Example of rendering](/doc/render.png)

## Installation

## Usage

### Write a .spod file

#### Syntax

Just write your help in a, for example, ```.spod```  file.

Use those commands to add colors:

- ```B<>``` for bold text
- ```D<>``` for default value
- ```O<>``` for options
- ```C<>``` for comment
- ```V<>``` for values
- ```T<>``` for (hash)tag

You can add __VARS__ in your spod. Specify ```-r VARS=value,OTHER=o``` to replace it on compilation

### Render it

#### Using external script

#### Using the shell function

## Examples

	NAME=magic.sh ./spod-parser.sh example.spod -r RANDOM,NAME

## License

GPL v2
