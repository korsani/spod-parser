UID:=$(shell id -u)
VIM:=$(shell vim --version| grep -E '^VIM' | head -1 | cut -d ' ' -f 1)
NVIM:=$(shell nvim --version| grep -E '^NVIM' | head -1 | cut -d ' ' -f 1)
# https://framagit.org/korsani/shell-compress
SHELL_COMPRESS:=$(shell command -v shell-compress)
OS:=$(shell uname)
PREFIX:=/usr/local
BIN_FILE:=spod-parser.sh
FUNC_FILE:=spod-parser.func.sh
VIM_SYNTAX_FILE=spod.vim
ifeq (0,$(UID))
DST_BIN_FILE:=$(PREFIX)/bin/spod-parser.sh
ifeq ($(OS),Darwin)
MAN_DIR5:=$(PREFIX)/man/man5
MAN_DIR1:=$(PREFIX)/man/man1
endif
ifeq ($(OS),Linux)
MAN_DIR5:=$(PREFIX)/share/man/man5
MAN_DIR1:=$(PREFIX)/share/man/man1
endif
endif

ifeq (NVIM,$(NVIM))
DST_VIM_SYNTAX_FILE+=~/.config/nvim/syntax/spod.vim
endif
ifeq (VIM,$(VIM))
DST_VIM_SYNTAX_FILE+=~/.vim/syntax/spod.vim
endif

ifneq (,$(SHELL_COMPRESS))
COMPRESSED_FUNC_FILE:=$(FUNC_FILE).sc
endif
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ifeq (0,$(UID))
install: $(DST_VIM_SYNTAX_FILE) $(DST_BIN_FILE) $(COMPRESSED_FUNC_FILE) man
else
install: $(DST_VIM_SYNTAX_FILE) $(COMPRESSED_FUNC_FILE)
endif

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

$(DST_VIM_SYNTAX_FILE): $(VIM_SYNTAX_FILE)
	install -D $< $@
	$(info * Add this somewhere in your vimrc / init.vim : 'au BufRead,BufNewFile *.spod set filetype=spod' *)

$(DST_BIN_FILE): $(BIN_FILE)
	install -D $< $@

$(COMPRESSED_FUNC_FILE): $(FUNC_FILE)
	shell-compress -qb < $< > $@

man: $(MAN_DIR5)/spod.5.gz $(MAN_DIR1)/spod-parser.1.gz

spod.5: spod.5.pod
	pod2man $< > $@

spod-parser.1: spod-parser.1.pod
	pod2man $< > $@

$(MAN_DIR5)/spod.5.gz: spod.5
	mkdir -p $(MAN_DIR5)
	gzip -c < $< > $@
	chmod 0644 $@

$(MAN_DIR1)/spod-parser.1.gz: spod-parser.1
	mkdir -p $(MAN_DIR1)
	gzip -c < $< > $@
	chmod 0644 $@

clean:
	rm -f $(COMPRESSED_FUNC_FILE)

uninstall: clean
	rm -f $(DST_VIM_SYNTAX_FILE) $(DST_BIN_FILE)
